//Arreglo sencillo
var Arreglo1 = ["Sebastian",16,"M"]
console.log(Arreglo1[1])

//Arreglo con otro areglo dentro
var Arreglo2 = ["Yulied",17,"F",["Matias",1,"M"]]
console.log(Arreglo2[3][0])

//Objeto con un arreglo dentro
var Objeto = {
    nombre : "Sebastian",
    edad : 45,
    sexo : "M",
    datos1 : [
        coche = {
            color : "Rojo",
            ruedas : 4,
            modelo : "1"
        },

        coche2 = {
            color : "Azul",
            ruedas : 4,
            modelo : "2"
        }
    ]
}

//Arreglo con un objeto dentro
var Arreglo3 = [Objeto,"Datos",1]

//Recorrer arreglo con objetos y arreglos dentro de esos objetos
console.log(Arreglo3[0].edad)
console.log(Arreglo3[0].datos1[1].color)