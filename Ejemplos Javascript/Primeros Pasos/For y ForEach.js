var array1 = ["Hola",2020,"Adios"]

var objeto1 = {
    nombre : "Sebastian",
    edad : 19,
    sexo : "M"
}

var objeto2 = {
    nombre : "Yulied",
    edad : 17,
    sexo : "F"
}

var objeto3 = {
    nombre : "Matias",
    edad : 1,
    sexo : "M"
}

var array2 = [objeto1,objeto2,objeto3]

//For Sencillo
for (i=0; i < 10; i++){

    console.log(`Hola mundo ${i} veces`)

}

//For con condicionales
for (i=10; i >= 0; i--){

    if (i == 1) {
        console.log(`Hola mundo ${i} vez`)
    }else{
        console.log(`Hola mundo ${i} veces`)
    }

}

//For para recorrer arreglos
for(i=0; i < array1.length; i++){
    const element = array1[i]
    console.log(element)
}

//For para recorrer objetos
for(i=0; i < array2.length; i++){
    const element = array2[i].nombre
    console.log(element)
}

//Recorrer arreglos
array2.forEach((datos, i) => {
    console.log(i,datos.nombre)
})