//var = Variables Globales
    var saludo = "Hola Mundo"
    console.log(saludo)
//let = Variables con scoope limitado a el bloque donde se use
    let nombre = "Sebastian"
    console.log(nombre)
//const = Variables inmutables, su valor es constante
    const pi = 3.14
    console.log(pi)


//Tipos de datos
    var numeros = 35
    var caracteres = "Prueba"
    var booleanos = true

    var nulo = null
    var indefinido = undefined


//Saber que de tipo es una variable
    console.log(typeof numeros)    