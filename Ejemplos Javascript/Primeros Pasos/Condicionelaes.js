var edad = 33

//Condicional Simple
if (edad >= 18){
    console.log("Adulto")
}else{
    console.log("Niño")
}

//Condicional Andidado
if (edad >= 0 && edad <= 17){
    console.log("Niño o Adolecente")
}else if (edad >= 18 && edad <= 33){
    console.log("Adulto Joven")
}else if (edad >= 34 && edad <= 60){
    console.log("Adulto")
}else{
    console.log("Anciano")
}