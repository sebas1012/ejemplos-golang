//Operadores
    var a = 25
    var b = 19
    var c = "25"

//Operadores Aritmeticos: +, -, *, /, %, **, ++, --
    var d = a + b
    console.log(d)

//Operadores de Comparacion: ==, !=, ===, !==, <=, >=, <, >
    //Hace comparacion entre valores sin importar su tipo, es decir si uno es number y otro string pero tienen el mismo valor es true ya que son lo mismo.
    console.log(a == c)
    //Hace comparacion entre valores donde estrictamente deben ser iguales como en valor y en tipo de dato.
    console.log(a === c)

//Operadores de Asignacion: =, +=, -=, *=, /=, %=, **=,
    a += 3
    console.log(a) 

//Operadores Logicos: &&, ||, !
    console.log(true && false)
    console.log(true || false)

//Operador terniario: condicion ? true : false
    var res= b >=18 ? "Mayor de Edad" : "Menor de Edad"
    console.log(res)