var a = 50
var b = 50.123

//Nos dice si un numore es entero o no
console.log(Number.isInteger(a))
//Vuelve un numero no entero a entero
console.log(Number.parseInt(b))
//Devuelve un numero decimal con la cantidad de numeros decimales que querramos
console.log(Number.parseFloat(b).toFixed(1))