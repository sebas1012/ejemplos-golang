var acceso = true

//Funcion flecha que retorna un solo valor dandole un parametro
var accesoU = a => a
console.log(accesoU(acceso))

//Funcion flecha que retorna un solo valor sin darle singun parametro
var accesoU1 = () => false
console.log(accesoU1())

//Funcion flecha que retorna un solo valor pasandole multiples parametros
var accesoU2 = (b,c) => console.log(`Usuario: ${b} Acceso: ${c}`)
accesoU2("Sebastian","Permitido")

//Funcion flecha que retorna mas de un valor pasandole multiples parametros
var accesoU3 = (d,e,f) => {
    console.log(`Usuario: ${d} Acceso: ${e}`)
    return f
}

//Operador Ternario que usa funcion accesoU3
accesoU3("Yulied","OK",acceso) == true ? console.log("Permitido") : console.log("Denegado")