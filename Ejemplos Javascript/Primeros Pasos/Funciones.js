var a1 = 1
var b1 = 2
var c1 = 3

function saludo(){
    console.log("Hello World!")
}

function resta(a,b){
    let c = a - b
    console.log(c)
}

var suma = function(a,b,c){
    return a + b + c
}

//Output
saludo()
resta(c1,b1)

//Si se usa un retun el output debe ser con console.log
console.log(suma(a1,b1,c1))