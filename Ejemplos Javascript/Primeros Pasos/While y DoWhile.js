var i = 3

// While con ciclo en reversa
while (i > 0) {
    i--
    console.log("Hola" + i)
}

var n = 0


//While con ciclo en aumento
while (n < 3) {
    n++
    console.log("Hola" + n)
}

var resultado = ""
var x = 0

console.log(typeof(resultado))


//Do while sencillo
do{
    x = x + 1
    resultado = resultado + x

}while (x < 5) {
    console.log(resultado)
}

