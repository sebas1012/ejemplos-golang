var nombre = "Sebastian"
var edad = "19"

//Formas de imprimir texto con variables: La segunda es la mas recomendada
console.log("Mi nombre es: " + nombre + "y tengo " + edad)
//Ese tipo de comillas se hacen con Alt+96
console.log(`Mi nombre es: ${nombre} y tengo ${edad}`)

//Nos devuelve toda la cadena en mayusculas
console.log(nombre.toUpperCase())
//Nos devuelve toda la cadena en minusculas
console.log(nombre.toLowerCase())
//Nos devuelve el tamaño de la cadena de texto
console.log(nombre.length)

