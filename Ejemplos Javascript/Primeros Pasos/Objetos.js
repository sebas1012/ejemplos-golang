var name = "Oscar"
var age = "17"
var sex = "M"

//Objeto sencillo
var Objeto1 = {
    //Clave : Valor
    nombre : "Sebastian",
    edad : "19",
    sexo : "M"    
}

console.log(Objeto1)
console.log(Objeto1.nombre)

//Objeto con otro objeto dentro
var Objeto2 = {
    importante : true,
    datos : {
        nombre : "Yulied",
        edad : 17,
        sexo : "F"
    } 
}

console.log(Objeto2)
console.log(Objeto2.datos.nombre)

//Objeto con interpolacion de texto
var Objeto3 = {
    data : `Nombre: ${name} Edad: ${age} Sexo: ${sex}`
}

console.log(Objeto3)

//Objeto con una funcion dentro
var Objeto4 = {
    funcion : (a , b) => a + b,
    fecha : new Date()
}

console.log(Objeto4.funcion(1 , 2))
console.log(Objeto4.fecha.getFullYear())

//Destructuracion de un objeto, sirve para mostrar datos especificos de un objeto como una variable.
var {importante} = Objeto2
console.log(importante)

//Desglozamiento de un objeto, sirve para mostrar datos de un objeto pero en forma de funcion
var Funcion2 = ({nombre}) => nombre
console.log(Funcion2(Objeto2.datos))

