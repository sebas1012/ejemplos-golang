# Funciones
def cero():
    return 0

print(cero())

def suma(a):
    return a + 1

print(suma(1))

def suma2(b, c):
    resultado = b + c
    return resultado

print(suma2(1, 2))


def fib(n):     
    a, b =0, 1
    while a != n:
        print(a, end=' ')    
        a, b = b, a+b
    print()


print(fib(35))