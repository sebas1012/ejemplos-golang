# Operadores de comparacion

a = 1 == 1
print(a)

a = 1 != 1
print(a)

a = 1 < 2
print(a)

a = 1 > 2
print(a)

a = 1 <= 2
print(a)

a = 1 >= 2
print(a)



# Operadores de Logicos

a = 1 == 1 and 2 == 2
print(a)

a = 1 == 1 or 2 == 3
print(a)

a = not 1 == 1

print(3 > 4 < 5 and 3 < 2 or 2 != 4 and 4 == 2 + 2 or not True)

print(12*5 == 6*10)