import math
import random

# Ejemplos libreria math

a = math.sqrt(9)
b = math.sin(7/2)

# print(a)
# print(b)

# Ejemplos libreria random

a = random.random()
b = random.choice([1,2,3])
c = random.choices(['Pera','Coco','Uva'], k=2)

print(a)
print(b)
print(c)
