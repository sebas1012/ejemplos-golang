# Ejemplos if

x = 1

# Sencillo
if x > 0 :
    print('Numero Positivo')

b = -3

# Alternando
if b > 0 :
    print('Numero Positivo')
else:
    print('Numero Negativo')

c = 2

# En una sola linea
print('Par') if c == 4 else print('Inpar')


# Encadenado
d = 2

if d < 0:
    print('Negativo')
elif d > 0:
    print('Positivo')
else:
    print('Cero')

# Anidado

if d < 0:
    print('Negativo')
else:
    if d > 0:
        print('Positivo')
    else:
        print('Cero')


# Ejemplo

x = 1+4*3+8/2*4+5**2
y = x%2

if y == 0:
    x+= 1
else:
    x+= 2
    
print(x)