# Ciclo While

# While se usa cuando no se sabe cuando terminara el ciclo, a diferencia del for que ya se sabe cuando acabara, por ello while va a compañado
# de una condicion

x = 1


while x < 10:
    x += 1
    print(x)

def multiplicacion (n):
    x = 0
    while x < 12:
        x += 1
        print(n, "x", x, "=", n * x)

multiplicacion(2)

x = 0
suma = 0

while x <= 100:
    x += 1

    c = x % 2

    if c == 0:  
        suma+=x
        print(suma)


n = 10

while n <= 10:

    print('Prueba')


s = 0

for n in range(10):

   s += n
   print(s)
